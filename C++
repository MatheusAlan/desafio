#include <iostream>
#include <cstdlib>
#include <cstring>
#include <set>


using namespace std;

/**
 * Classe que abstrai um Gato. 
 */
class Gatos {
private:
    string raca;
    string pais;
    int peso;
    int vida;

public:

    /** 
     * Implementação do Construtor sem parâmetro
     */
    Gatos(){
    	string raca = "";
    	string pais = "";
    	int peso = 0;
    	int vida = 0;
    }

    /**
     * Implementação do Construtor com parâmetros
     * @param raca Raça do Gato.
     * @param pais País de origem do Gato.
     * @param peso Peso Médio do Gato.
     * @param vida Vida média do Gato.
     */
    Equipamento(string raca, string pais, int peso, int vida) {
        setRaca(raca);
        setPais(pais);
        setPeso(peso);
        setVida(vida);

    }
    //Get´s e Set´s

    /**
     * Recuperador da Raça.
     * @param nome um valor inteiro a ser atribuído para a raça do gato.
     */
    string getRaca() {
        return raca;
    }

    /**
     * Modificador da Raça.
     * @param nome um valor inteiro a ser atribuído para a raça do gato.
     */
    void setRaca(string raca) {
        this->raca = raca;
    }

    /**
     * Recuperador do País.
     *  @return uma string com o país do gato.
     */
    string getPais() {
        return pais;
    }

    /**
     * Modificador de País.
     *@param o curso uma string a ser atribuido para o país de algum gato.
     */
    void setPais(string pais) {
        this->pais = pais;
    }

    /**
     * Recuperador de Peso.
     * @return um inteiro com o peso do gato.
     */
    int getPeso() {
        return peso;
    }

    /**
     * Modificador de Peso
     * @param o curso um valor inteiro a ser atribuido para o peso de algum gato.
     */
    void setPeso(int peso) {
        this->peso = peso;
    }
    
    /**
     *Recuperador de Vida
     *@param o curso um valor inteiro a ser atribuido para a vida de algum gato.
     */
    int getVida(){
    	return vida;
	}
	/**
     *Modificador de Vida
     *param fase um valor inteiro a ser atribuido para a vida
     */
	void setVida (int vida){
		this -> vida = vida;
	}
};

//Classe que abstrai uma Moradia. 

class ControleGato {
private:
    Gatos* gatos[100];
    int quantidade;
public:

    //Implementação do Construtor sem parâmetro
    ControleEquipamento() {
        setQuantidade(0);
    }

    //Recuperador de Quantidade
    int getQuantidade() {
        return quantidade;
    }

    //Modificador de quantidade
    void setQuantidade(int quantidade) {
        this->quantidade = quantidade;
    }

    //Recuperador de Gatos
    Gatos* getGatos(int x) {
        if (x <= getQuantidade()) {
            return gatos[x];
        } else {
            return NULL;
        }
    }

    //Modificador de gato
    void setGato(Gatos* gato) {
        //Coloca o gato no vetor
        gatos[getQuantidade()] = gato;
        //Incrementa a quantidade de gatos
        setQuantidade(getQuantidade() + 1);
    }

    //Retorno de códigos no País
    int getQuantidadePais(string pais) {
        int qtdepais = 0;
        //Percorre o vetor de gato
        for (int x = 0; x < getQuantidade(); x++) {
            if (gatos[x]->getPais() == pais) {
                qtdepais = qtdepais + 1;
            }
        }
        return qtdepais;
    }
    
    //Atualiza utilizando a chave
    void AtualizarRaca (string raca){
    	int i;
        int AltPeso, AltVida;
        string AltPais, AltRaca;
        //Percorre o vetor do gato
        for(i = 0; i < getQuantidade(); i++){
            Gatos* Gato = getGatos(i);
            //Procura o valor de correspondente ao da raça desejada
            if (Gato->getRaca() == raca) {
                cout << "Encontrado" << endl;
                cout << "Digite o novo Raça:";
                getline(cin,AltRaca);
                cout << "Digite o novo País:";
                getline(cin,AltPais);
                cout << "Digite o novo Peso Médio:";
                cin >> AltPeso;
                cout << "Digite a nova Vida Média:";
                cin >> AltVida;
                //Preenche o objeto
                Gato->setRaca(AltRaca);
                Gato->setPais(AltPais);
                Gato->setPeso(AltPeso);
                Gato->setVida(AltVida);
			}
		}
	}
    //Atualiza utilizando o País	
	void AtualizarPais (string pais){
        int i;
        int AltPeso, AltVida;
        string AltPais, AltRaca;
        //Percorre o vetor do gato
        for(i = 0; i < getQuantidade(); i++){
            Gatos* Gato = getGatos(i);
            //Procura o valor de correspondente ao do país desejado
            if (Gato->getPais() == pais) {
                cout << "Encontrado" << endl;
                cout << "Digite o novo Raça:";
                getline(cin,AltRaca);
                cout << "Digite o novo País:";
                getline(cin,AltPais);
                cout << "Digite o novo Peso Médio:";
                cin >> AltPeso;
                cout << "Digite a nova Vida Média:";
                cin >> AltVida;
                //Preenche o objeto
                Gato->setRaca(AltRaca);
                Gato->setPais(AltPais);
                Gato->setPeso(AltPeso);
                Gato->setVida(AltVida);
			}
		}
	}
	 //Verifica se a Raça desejado já existe
	int TesteRaca (string raca){
		int teste = 0;
        //Percorre o vetor de gatos
        for (int x = 0; x < getQuantidade(); x++) {
            if (gatos[x]->getRaca() == raca) {
                teste = teste + 1;
            }
        }
        return teste;
    }
    
    //Lista o vetor de gato
    void Listar (){
    	int x;
        int i;
        Gatos* Gato;
        //Percorre o vetor de gato
        if(getQuantidade() > 0){
        	for(i = 0; i < getQuantidade() ; i++){
        		Gato = getGatos(i);
        		//Mostra na tela
        		cout << i+1 << "°" << endl;
            	cout << "A raça do gato é  " << Gato->getRaca()<< ";"<< endl;
            	cout << "O país de sua origem é " << Gato->getPais() << ";" << endl;
            	cout << "O peso médio é de " << Gato->getPeso() << " Kg;"<< endl;
            	cout << "A vida média é de " << Gato->getVida() << " anos." << endl;
        	}
		}else{
			cout << "Não posssui nenhum cadastro." << endl;
		}
	}
    
	
    bool DeletarRaca(string valor){
 		if (getQuantidade() != 0) {
        	int j = 0;
        	//Procura a posição da Raça no controle.
        	//Compara o valor no gato ou até chegar no final do controle.
        	while ((j < getQuantidade()) && (valor != gatos[j]->getRaca())) {
        	    j = j + 1;
        	}
        	//Verifica se a posição esta dentro do intervalo do gato
        	if (j < getQuantidade()) {
        	    //Desaloca o elemento da posição j
        	    delete gatos[j];
        	    //Desloca os gatos do fim até a posição j do controle
        	    for (int i = j; i < getQuantidade(); i++) {
        	        gatos[i] = gatos[i + 1];
        	    }
        	    //Decrementa a quantidade de gatos do controle.
       		    setQuantidade(getQuantidade() - 1);
       		    
       		    cout << "Deletado com sucesso" << endl;
         		return true;
        	} else {
            	cout << "Excluir valor - Não existe este código" << endl;
            	return false;
        	}
    	} else {
        cout << "Excluir valor - Não possui nenhum cadastro" << endl;
        return false;
    	}
	}
	
	bool DeletarPais(string valor){
	 	if (getQuantidade() != 0) {
        	int j = 0;
        	//Procura a posição do País no controle.
        	//Compara o valor no gato ou até chegar no final do controle.
        	while ((j < getQuantidade()) && (valor != gatos[j]->getPais())) {
        	    j = j + 1;
        	}
        	//Verifica se a posição esta dentro do intervalo do controle
        	if (j < getQuantidade()) {
        	    //Desaloca o elemento da posição j
        	    delete gatos[j];
        	    //Desloca os nós do fim até a posição j do controle.
        	    for (int i = j; i < getQuantidade(); i++) {
        	        gatos[i] = gatos[i + 1];
        	    }
        	    //Decrementa a quantidade de gatos do controle.
       		    setQuantidade(getQuantidade() - 1);
       		    
       		    cout << "Deletado com sucesso" << endl;
         		return true;
        	} else {
            	cout << "Excluir valor - Não existe este local" << endl;
            	return false;
        	}
    	} else {
        cout << "Excluir valor - Não possui nenhum cadastro" << endl;
        return false;
    	}
	}

	
};

int main() {

    ControleGato* controlegato = new ControleGato();
	setlocale(LC_ALL, "Portuguese");

    //Variável que define as opções do menu
    int menu = -1;
    while (menu != 11) {
        //Escreve o menu de opções
        cout << endl << ">> Criação e modificação de banco de dados<<"
                << endl << " 1 - Incluir Raça"
                << endl << " 2 - Consultar Raça"
                << endl << " 3 - Contar Raça"
                << endl << " 4 - Contar País"
                << endl << " 5 - Listar"
                << endl << " 6 - Consultar País"
                << endl << " 7 - Alterar com Raça"
                << endl << " 8 - Alterar com País"
                << endl << " 9 - Excluir por Raça"
                << endl << " 10 - Excluir por País"
                << endl << " 11 - Sair"
                << endl << " Digite uma opção:";
        //Lê a opção de entrada
        cin >> menu;
        switch (menu) {
            case 1:
            {
                Gatos* gatos = new Gatos();
                int peso, vida;
                string pais, raca;
                cout << endl << "Digite a Raça: ";
                cin.ignore();
                getline(cin,raca);
                if(controlegato->TesteRaca(raca) == 0){
                	cout << "Digite o país de origem da raça: ";
                	getline(cin, pais);
               		cout << "Digite o peso médio: ";
                	cin >> peso;
                	cout << "Digite a vida média: ";
                	cin >> vida;
                	//Preenche o objeto
                	gatos->setRaca(raca);
                	gatos->setPais(pais);
                	gatos->setPeso(peso);
                	gatos->setVida(vida);
                	//Seta o gato na moradia
                	controlegato->setGato(gatos);
				}else{
					cout << "Este código já existe" << endl;
				}
                break;
            }
            case 2:
            {
                if (controlegato->getQuantidade() != 0) {
                    cout << endl << "Consulta:" << endl;
                    int i;
                    string raca;
                    cout << "Digite a Raça: ";
                    cin.ignore();
                    getline(cin,raca);
                    for(i = 0; i < controlegato->getQuantidade(); i++){
                    	Gatos* gatos = controlegato->getGatos(i);
                    	if (gatos->getRaca() == raca) {
                    		//Procura o gato na posição
       					    cout << "A raça do gato é  " << gatos->getRaca()<< ";"<< endl;
        					cout << "O país de sua origem é " << gatos->getPais() << ";" << endl;
            				cout << "O peso médio é de " << gatos->getPeso() << " Kg;"<< endl;
            				cout << "A vida média é de " << gatos->getVida() << " anos." << endl;
						}
                	}
				}
                break;
           	}
            case 3:
            {
            	//Mostra os gatos que posssui na Moradia
                cout << endl << "Quantidade de raças no banco de dados: " << controlegato->getQuantidade() << endl;
                break;
            }
            case 4:
            {
                string pais;
                cout << "Digite o país que deseja saber as raças: ";
                cin.ignore();
                getline(cin, pais);
                // Mostra quantos gatos tem no local
                cout << endl << "Quantidade de raças naquele país " << pais << " : " << controlegato->getQuantidadePais(pais) << endl;
                break;
            }
            case 5:
        	{
				controlegato->Listar();
				break;
			}
			case 6:
			{
                if (controlegato->getQuantidade() != 0) {
                    cout << endl << "Consulta:" << endl;
                    int i;
                    string pais;
                    cout << "Digite o país da raça: ";
                	cin.ignore();
                	getline(cin, pais);
                    for(i = 0; i < controlegato->getQuantidade(); i++){
                    	Gatos* gatos = controlegato->getGatos(i);
                    	if (gatos->getPais() == pais) {
                    		//Procura o gato na posição
       					    cout << "A raça do gato é  " << gatos->getRaca()<< ";"<< endl;
        					cout << "O país de sua origem é " << gatos->getPais() << ";" << endl;
            				cout << "O peso médio é de " << gatos->getPeso() << " Kg;"<< endl;
            				cout << "A vida média é de " << gatos->getVida() << " anos." << endl;
						}
                	}
				}
                break;				
			}
			case 7:
			{
				if (controlegato->getQuantidade() != 0) {
                    cout << endl << "Alterar:" << endl;
                    string raca;
                    cout << "Digite a raça que deseja alterar: ";
                    cin.ignore();
                    getline(cin,raca);
					controlegato->AtualizarRaca(raca);
				}
				break;
			}
			case 8:
			{
				if (controlegato->getQuantidade() != 0) {
                    cout << endl << "Alterar:" << endl;
                    string pais;
                    cout << "Digite o país que deseja alterar: ";
                    cin.ignore();
                    getline(cin, pais);
                    controlegato->AtualizarPais(pais);
				}
				break;
			}
			case 9:
			{
				string valor;
				cout << "Digite a raça que deseja excluir:";
				cin.ignore();
				getline(cin,valor);
				controlegato->DeletarRaca(valor);
				break;
			}
			case 10:
			{
				string pais;
				cout << "Digite o país que deseja excluir:";
				cin.ignore();
				getline(cin, pais);
				controlegato->DeletarPais(pais);
				break;
			}
            case 11:
                cout << endl << "Saindo do sistema";
                break;
            default:
                cout << endl << "Opção inválida";
        }
    }
    return EXIT_SUCCESS;
}